# Week 2 Mini Project
> Oliver Chen (yc557)
## Lambda functionality

This code defines a simple AWS Lambda function written in Rust to calculate the triple of a given number. The function is designed to be invoked as an AWS Lambda function, handling events asynchronously. It uses the `lambda_runtime` and `tokio` libraries for runtime and asynchronous processing, respectively. The request payload is expected to contain a 'body' field, which is then parsed into a HashMap representing a number. The function calculates the cube of this number, constructs a JSON response, and returns it with a 200 status code. The response includes the calculated triple and appropriate headers for JSON content. This code serves as a basic example of AWS Lambda implementation in Rust, showcasing event handling, payload parsing, and response generation.

## API Gateway integration
The Lambda function is integrated with API gateway on AWS. The gateway URL is https://ixxecz9ms5.execute-api.us-east-1.amazonaws.com/default/triple and it can be tested using Postman as follows:

![Image 1](/images/1.png)

Here are some screenshots from AWS about the API Gateway:

![Image 2](/images/2.png)

![Image 3](/images/3.png)

## Data processing
The data processing aspect of this code involves deserializing incoming Lambda event payloads and extracting relevant information for computation, specifically calculating the triple (cube) of a number. The Request structure represents the payload, with the 'body' field containing a JSON string. The code uses serde to deserialize this JSON into a Request structure, and then further deserializes the 'body' field into a HashMap representing key-value pairs. Specifically, it expects a 'number' field within the 'body'. Any errors during deserialization are logged, and appropriate error handling is applied. The extracted 'number' is then used to calculate the triple, which is subsequently included in the Response structure. The final response is serialized into a JSON string. This demonstrates a robust data processing flow, where the core processing involves the calculation of the triple/cube, and it showcases the handling of serialization and deserialization operations to manipulate the input data before generating the appropriate output for the Lambda function's response.

## Documentation
A Makefile is included for running commands easier. Specifically:

### Build stage
```
make build
```

or

```
cargo lambda build --release --arm64
```

### Deploy stage
```
make deploy
```

or

```
cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::471112660632:role/ids721
```

### Test stage
We can either use Postman or cURL to test the deployed app.

```
curl -X POST https://ixxecz9ms5.execute-api.us-east-1.amazonaws.com/test/triple -H "Content-Type: application/json" -d '{"number":4}'
```
Here is the output:

![Image 4](/images/4.png)