rust-version:
	@echo "Rust command-line utility versions:"
	rustc --version 			#rust compiler
	cargo --version 			#rust package manager
	rustfmt --version			#rust code formatter
	rustup --version			#rust toolchain manager
	clippy-driver --version		#rust linter

format:
	cargo fmt --quiet

lint:
	cargo clippy --quiet

test:
	cargo test --quiet

#### Cargo Lambda Section ####
## Watches for changes and rebuilds
watch:
	# cargo lambda watch
	cargo lambda watch -p 8080 -a 127.0.0.1

invoke:
	cargo lambda invoke \
		--data-ascii '{"name": "Oliver"}' \
		--output-format json

### Build for AWS Lambda (use arm64 for AWS Graviton2)
build:
	cargo lambda build --release --arm64

deploy:
	cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::471112660632:role/ids721

### Invoke on AWS
aws-invoke:
	cargo lambda invoke --remote week2-mini-project --data-ascii '{"name": "aaabbbbbcccccccc"}' \
		--output-format json

run:
	cargo run

release:
	cargo build --release

all: format lint test run