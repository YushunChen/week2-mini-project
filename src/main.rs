// Import necessary dependencies
use lambda_runtime::{service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use std::collections::HashMap;

// Define a structure to represent the request payload
#[derive(Deserialize)]
struct Request {
    body: String,
}

// Define a structure to represent the response payload
#[derive(Serialize)]
struct Response {
    status_code: u16,
    headers: HashMap<String, String>,
    body: String,
    is_base64_encoded: bool,
}

// Asynchronous function to handle Lambda events
async fn function_handler(event: LambdaEvent<Value>) -> Result<Response, Error> {
    // Deserialize the Lambda event payload into a Request structure
    let request: Request = serde_json::from_value(event.payload).map_err(|e| {
        eprintln!("Error parsing event payload: {}", e);
        Error::from(e.to_string())
    })?;

    // Deserialize the 'body' field of the Request into a HashMap
    let parsed_body: HashMap<String, i32> = serde_json::from_str(&request.body).map_err(|e| {
        eprintln!("Error parsing number from body: {}", e);
        Error::from(e.to_string())
    })?;

    // Extract the 'number' field from the parsed body
    let number = match parsed_body.get("number") {
        Some(num) => *num,
        None => return Err(Error::from("No number field in body")),
    };

    // Calculate the triple of the number
    let triple = number * number * number;

    // Create a Response structure with the calculated triple
    let response = Response {
        status_code: 200,
        headers: [("Content-Type".to_string(), "application/json".to_string())]
            .iter().cloned().collect(),
        body: json!({ "triple": triple }).to_string(),
        is_base64_encoded: false,
    };

    // Return the Response structure as a Result
    Ok(response)
}

// Asynchronous main function to run the Lambda function
#[tokio::main]
async fn main() -> Result<(), Error> {
    // Create a service function from the function_handler
    let func = service_fn(function_handler);

    // Run the Lambda function using the lambda_runtime
    lambda_runtime::run(func).await?;

    // Return Ok if the Lambda function runs successfully
    Ok(())
}
